# Helm Charts

## Usage

[Helm](https://helm.sh) must be installed to use the charts.  Please refer to
Helm's [documentation](https://helm.sh/docs) to get started.

Once Helm has been set up correctly, add the repo as follows:

    helm repo add hoooki https://gitlab.com/api/v4/projects/53477465/packages/helm/stable

If you had already added this repo earlier, run `helm repo update` to retrieve
the latest versions of the packages.  You can then run `helm search repo
hoooki` to see the charts.

To install the <chart-name> chart:

    helm install my-release hoooki/<chart>

To uninstall the chart:

    helm delete my-release

## Beta

Beta repository contains non-fully tested Helm charts. Use them with care, we do not guarantee bug-free operation.

    helm repo add hoooki-beta https://gitlab.com/api/v4/projects/53477465/packages/helm/beta