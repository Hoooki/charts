# WEB-APP - 0.1.1-beta
A Helm chart for Web applications
---

# Helm Charts

## Usage

[Helm](https://helm.sh) must be installed to use the charts.  Please refer to
Helm's [documentation](https://helm.sh/docs) to get started.

Once Helm has been set up correctly, add the repo as follows:

    helm repo add --username <username> --password <access_token> hoooki https://gitlab.com/api/v4/projects/53477465/packages/helm/stable

If you had already added this repo earlier, run `helm repo update` to retrieve
the latest versions of the packages.  You can then run `helm search repo
<alias>` to see the charts.

To install the web-app chart:

    helm install my-web-app hoooki/web-app

To uninstall the chart:

    helm delete my-web-app

## Beta

Beta repository contains non-fully tested Helm charts. Use them with care, we do not guarantee bug-free operation.

    helm repo add hoooki-beta https://gitlab.com/api/v4/projects/53477465/packages/helm/beta

| Parameter | Default | Comment |
|---|---|---|
|`global.replicaCount`|1|Global number of replicas|
|`global.imagePullSecrets`|[]|Global image pull secrets|
|`global.nameOverride`|""|Chart name override|
|`global.fullnameOverride`|""|Default fully qualified app name.|
|`global.imagePullPolicy`|"IfNotPresent"|Global pull policy|
|`global.podAnnotations`|{}|Global pod annotations|
|`global.podLabels`|{}|Global pod labels|
|`global.podSecurityContext.fsGroupChangePolicy`|"OnRootMismatch"||
|`global.volumes`|[]|Additional volumes on the output Deployment definition.|
|`global.serviceAccount.create`|true|Create a service account|
|`global.serviceAccount.automount`|true|Automatically mount a ServiceAccount's API credentials?|
|`global.serviceAccount.annotations`|{}|Annotations to add to the service account|
|`global.serviceAccount.name`|""|The name of the service account to use. If not set and create is true, a name is generated using the fullname template|
|`global.autoscaling.enabled`|false|Enable / Disable autoscaling|
|`global.autoscaling.minReplicas`|1|Minimum replicas|
|`global.autoscaling.maxReplicas`|100|Maximum replicas|
|`global.autoscaling.targetCPUUtilizationPercentage`|80||
|`global.nodeSelector`|{}|Node selector|
|`global.tolerations`|[]|Tolerations|
|`global.affinity`|{}|Node affinity|
|`application.enabled`|true|Enable / Disable 'application' container|
|`application.image.repository`|"php:fpm"|'application' image|
|`application.image.pullPolicy`|""|Overrides global pullPolicy|
|`application.image.tag`|""|image's tag|
|`application.entrypoint.args`|[]|Pass args to the 'application' container|
|`application.entrypoint.command`|[]|Run specific command in the 'application' container|
|`application.env`|{}||
|`application.securityContext`|{}|'application' security container|
|`application.resources.limits.cpu`|"100m"||
|`application.resources.limits.memory`|"128Mi"||
|`application.resources.requests.cpu`|"100m"||
|`application.resources.requests.memory`|"128Mi"||
|`application.livenessProbe`|{}|Enable / Disable livenessProbe|
|`application.readinessProbe`|{}|Enable / Disable readinessProbe|
|`application.configMaps`|[]|Additional configMaps|
|`application.volumeMounts`|null|Additional volumeMounts on the output Deployment definition.|
|`webserver.enabled`|true|Enable / Disable 'webserver' container|
|`webserver.image.repository`|"nginxinc/nginx-unprivileged"|'webserver' image|
|`webserver.image.pullPolicy`|""|Overrides global pullPolicy|
|`webserver.image.tag`|"alpine3.18"|image's tag|
|`webserver.env`|{}||
|`webserver.container.port`|8080|Container exposed port|
|`webserver.securityContext`|{}|'webserver' security context|
|`webserver.resources.limits.cpu`|"50m"||
|`webserver.resources.limits.memory`|"64Mi"||
|`webserver.resources.requests.cpu`|"50m"||
|`webserver.resources.requests.memory`|"64Mi"||
|`webserver.livenessProbe`|{}|Enable / Disable livenessProbe|
|`webserver.readinessProbe`|{}|Enable / Disable readinessProbe|
|`webserver.volumeMounts`|[]|Additional volumeMounts on the output Deployment definition.|
|`webserver.configMaps`|[]|Additional configMaps|
|`service.type`|"ClusterIP"|Service type|
|`service.port`|80|Service port|
|`service.name`|""|Overrides default service|
|`ingress.enabled`|false|Enable / Disable ingress|
|`ingress.className`|""|Ingress class name|
|`ingress.annotations`|{}||
|`ingress.hosts`|[{"host": "chart-example.local", "paths": [{"path": "/", "pathType": "ImplementationSpecific"}]}]||
|`ingress.tls`|[]|Ingress TLS|
|`additionalContainers`|[]|Add containers (as plain object)|
|`additionalConfigMaps`|[]|Add configMap (as plain object)|
|`initContainers`|[]|Inject init containers|

