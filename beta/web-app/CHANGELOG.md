# Changelog

All notable changes to this project will be documented in this file.

## [0.1.1-beta] 2024-02-14

### Features

- Added more flexibility in the configuration with additional ConfigMap, InitContainer, ...

### BugFixes

- Fixed many small bugs, templating errors, ...

## [0.1.0-beta] 2024-02-02

### Features

- Add (beta) web-app chart

