{{- define "web-app.service.name" -}}
{{- if .Values.service.name }}
{{- printf "%s" .Values.service.name }}
{{- else }}
{{- default .Release.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}