#!/bin/bash

logPrefix="Charts"

function log() {
  local level=$1
  local message=$2
  local date=$(date)

  echo "[$date] $logPrefix.$level : $message"
}

function info() {
  log "INFO" "$1"
}
function warning() {
  log "WARNING" "$1"
}
function error() {
  log "ERROR" "$1"
  exit 1
}

if [ $# -gt 2 ]; then
  error "Usage : ./doc.sh <chart> <version>"
fi

CHART=$1
VERSION=$2

info "Generating README.md"

frigate gen "$1" > "$1"/README.md